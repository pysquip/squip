*********
Changelog
*********

Changes grouped by minor versions and patches.

Version 0.2
===========

0.2.2 # [current development version]
-------------------------------------
- Bugfix: fixes bug remaining in node range analysis
- Bugfix: runtime detection fixed (#3)
- Bugfix: fixes job -> node assignment in ranges with more than single didget numbers (#2)
- Bugfix: implements node range analysis for jobs 'nodelist' field (#1)

0.2.1 # 2021-05-05
------------------
- Feature: CLI flag to exclude all jobs from the output
- BugFix: identification of nodes that are down in 'get_color'
- BugFix: colorcode 'low load' entry added and colors adjusted

0.2.0 # 2021-04-18
------------------
first beta release