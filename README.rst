*****************************
SQuIP - Slurm Queue In Pretty
*****************************

Purpose of this package
=======================

prettier cluster and queue monitoring for SLURM based systems.

.. image:: images/cluster_user_small.png

Installation
============

This package is NOT available on PyPI and has to be installed through a local copy of the source code.

With a local copy of this package present, cd to the directory containing the ``setup.py`` and ``setup.cfg`` files and the ``squip`` directory with all the ``.py`` files and run

.. code-block:: sh

    pip install -e .

If you want to install the package only for the current user run

.. code-block:: sh

    pip install -e . --user

and if multiple python/pip versions are present ensure installation with the desired environment using something like

.. code-block:: sh

    python3 -m pip install -e .

instead.

.. warning::

    It is recommended to install this as an editable package (use ``-e`` flag with pip install) since it is still an alpha version and frequent bugfixes might be necessary.


Usage
=====

After the successfull installation of ``squip`` the entry point with the same name should be available.

Run

.. code-block:: sh

    squip

to get information about the cluster, nodes, jobs and resources.
A list of all available flags is given when

.. code-block:: sh

    squip --help

is used.


Customizing - QA
================

How to provide default settings?
--------------------------------
To provide default settings ``.squiprc`` can be used. Two paths are checked for configuration files automatically:

#. the 'global' path
    ``/etc/.squiprc``

#. and the users home directory
    ``~/.squiprc``

Those files have to be written in JSON. The available keys are mentioned in the following part of this QA section.

The order in which settings are applied or loaded is the following:

- default settings from squip source code
- settings given in /etc/.squiprc
- settings given in ~/.squiprc
- command line flags used in CLI

.. note:: JSON - Syntax

    Enclose all key value pairs in curly braces ``{...}``.

    Place statements as key value pairs ``"key": "value"``

    Remember to use "," to seperate entries in the JSON file, but do not leave a trailing "," after the last pair.

    If a key contains key value pairs itself use nested ``{...}`` and ``[...]`` for value lists.


How to change the color scheme?
-------------------------------
the mutually exclusive flags ``--dark`` vs. ``--light`` and ``--user`` vs. ``--root`` are available for predefined color setups (default is dark/user).
To change the default color scheme add the following lines to your ``.squiprc`` file

.. code-block:: json

    {
        "scheme": "light",
        "user-scheme": "root"
    }


Explicit colors for each key (see ``squip_base`` for a complete list of keys) can be defined as well using the ``colors`` key.
The ANSI-Escape-Sequence number must be used and depending on the terminal might not always be supported.

.. code-block:: json

    {
        "colors": {
            "color_key": "xxx",
            "another_key": "..."
        }
    }

.. note::

    The base scheme is ALWAYS loaded first and will be overwritten by colors explicitly defined within a ``.squiprc`` file, hence, overwriting the CLI flags in this case contrary to the usual behaviour.


How to select nodes?
--------------------
use ``--nodes`` to pass a regex that would match the desired nodes or supply a user settings file with a ``nodes`` attribute that has a list of regex strings as values. In the example every node starting with ``name`` would be included.

.. code-block:: json

    {
        "nodes": ["name.*"]
    }

This might be a very important setting, since switches and other hardware that is not a host to jobs and does not have the necessary attributes might cause the script to crash with a KeyError.


How to suppress empty nodes?
----------------------------
use the ``--no-empty`` flag or include the ``empty_nodes`` attribute in your ``.squiprc`` file with the value ``false``

.. code-block:: json

    {
        "empty_nodes": false
    }


How to show only my jobs?
-------------------------
use the ``--users-jobs-only`` or ``-u`` flag for single use or put the following line in you ``.squiprc`` file:

.. code-block:: json

    "users_jobs_only": true


How to hide the pending jobs?
-----------------------------
use the ``--no-pending`` flag.


How to hide the cluster status report?
--------------------------------------
use the ``--no-status`` flag.


How to show only pending jobs?
------------------------------
use the ``--pending`` flag.


How to show only the status report?
-----------------------------------
use the ``--status`` flag.


Trubleshooting
==============

Problems related to json
------------------------

There is probably a syntax error within the global or local ``.squiprc`` file. Typical mistakes are

#. missing or trailing "," characters

#. using "=" instead of ":"


.. include:: CHANGES.rst
