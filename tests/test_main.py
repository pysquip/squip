"""Run a mocked test of nodestat."""

from typing import List
from unittest.mock import MagicMock, patch
import pytest

from squip import main as sq_main

mock_inp_path = 'tests/mock_sources'


def get_node_data(mockflag: bool) -> List[str]:
    """Mock getting info on nodes with scontrol."""
    with open(f'{mock_inp_path}/scontrol_nodes.txt', 'r') as inp_file:
        content = inp_file.readlines()
    return content


def get_job_data(mockflag: bool) -> List[str]:
    """Mock getting info on nodes with scontrol."""
    with open(f'{mock_inp_path}/scontrol_jobs.txt', 'r') as inp_file:
        content = inp_file.readlines()
    return content


@pytest.mark.parametrize(
    'flags', [
        ('--light', '--root',),
        ('--light', '--user'),
        ('--dark', '--root'),
        ('--dark', '--user',),
        ('--dark', '--user', '--no-empty'),
        ('--dark', '--user', '--no-status'),
        ('--dark', '--user', '--no-pending'),
        ('--dark', '--user', '--no-jobs'),
        ('--dark', '--user', '--users-jobs-only'),
        ('--dark', '--user', '-f', 'tests/mock_sources/squiprc'),
        ('-f', 'tests/mock_sources/squiprc2'),
        ('-f', 'tests/mock_sources/squiprc3'),
    ]
)
@pytest.mark.parametrize(
    'usr', ['root', 'user1', 'user2']
)
@patch('squip.cluster.Cluster.get_job_data', side_effect=get_job_data)
@patch('squip.cluster.Cluster.get_node_data', side_effect=get_node_data)
def test_full(
    mock_get_node_data: MagicMock,
    mock_get_job_data: MagicMock,
    usr: str,
    flags: str,
) -> None:
    """Integration test."""
    print()
    const = ('--nodes', 'node.*', '--mocked')
    with patch('sys.argv', new=['squip', *flags, *const]):
        with patch('squip.nodestat.get_user', new=lambda: usr):
            sq_main()
