# tests for utils module
from squip.utils import resolve_node_range
import pytest


@pytest.mark.parametrize(
    'nodelist, expected',
    [
        ('node-[1]', 'node-1|'),
        ('node-[1-4]', 'node-1|node-2|node-3|node-4|'),
        ('n-4-[5-6,8-12]', 'n-4-10|n-4-11|n-4-12|n-4-5|n-4-6|n-4-8|n-4-9|'),
        ('n-1-[1-2],n-4-[5-6,8-12]', 'n-1-1|n-1-2|n-4-10|n-4-11|n-4-12|n-4-5|n-4-6|n-4-8|n-4-9|')
    ]
)
def test_resolved_node_range(nodelist: str, expected: str) -> None:
    assert expected == resolve_node_range(nodelist)
