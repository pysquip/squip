"""Provide Setting class."""

from typing import Dict, Iterable, List, Optional
from argparse import Namespace
import pathlib
import json

from . import utils

SCHEME_CHOICES = ('dark', 'light')
USER_CHOICES = ('user', 'root')


class Warnings:
    """Class for handling squirc related warnings."""

    prefix = "WARNING:  "
    padding = " "*len(prefix)


class UnknownKeyWarning(Warnings):
    """Class for handlung misspelled/unknown keys."""

    def __init__(self, key: str) -> None:
        """Initialize warning when an unregistered key is used."""
        self.key = key

    def __str__(self) -> str:
        """Return human-friendly text with warning."""
        return f'{self.prefix}Unknown key "{self.key}" (will be ignored)'


class InvalidKeyValueWarning(Warnings):
    """Class for handlung misspelled/unknown values for keys."""

    def __init__(
        self,
        key: str,
        val: str,
        valids: Optional[Iterable[str]] = None
    ):
        """Initialize warning when an invalid value is used."""
        self.key = key
        self.val = val
        self.valids = valids

    def __str__(self) -> str:
        """Return human-friendly text with warning."""
        string = (
            f'{self.prefix}Invalid value "{self.val}" for key "{self.key}"\n'
        )
        if self.valids is not None:
            string += f'{self.padding} options are '
            string += "|".join(str(i) for i in self.valids)
            string += '\n'
        string += self.padding + "key will be ignored."
        return string


class Setting:
    """Setting class for settings storage."""

    def __init__(
        self,
        args: Namespace,
    ) -> None:
        """Initialize Setting object."""
        self.scheme: str = ''
        self.user_scheme: str = ''
        self.as_root: bool = False
        self.nodes: List[str] = []
        self.pending = True
        self.status = True
        self.debug = False
        self.empty_nodes = True
        self.mine_only = False
        self.print_nodes = True
        self.show_jobs = True
        self.user = ''
        self.colors: Dict[str, str] = {}
        self.logger = utils.dummy_logger
        self.logging = False

        self.load_global_settings()
        self.load_user_settings(args)
        self.set_as_root(args)
        self.set_scheme(args)
        self.set_nodes(args)
        self.set_selectors(args)
        self.set_logging(args)
        self.set_debug(args)
        self.set_mocked(args)

        if len(self.nodes) == 0:
            self.nodes = ['.*']

    def set_scheme(
        self,
        args: Namespace,
    ) -> None:
        """Set color scheme."""
        try:
            self.scheme = [
                key for key, val in vars(args).items()
                if val and key in SCHEME_CHOICES
            ][0]
        except IndexError:
            if self.scheme == '':
                self.scheme = 'dark'

        if self.as_root:
            return

        try:
            self.user_scheme = [
                key for key, val in vars(args).items()
                if val and key in USER_CHOICES
            ][0]
        except IndexError:
            if self.user_scheme == '':
                self.user_scheme = 'user'

    def set_as_root(self, args: Namespace) -> None:
        """Set as_root attribute."""
        if args.as_root:
            self.user_scheme = 'root'
            self.as_root = True

    def set_nodes(self, args: Namespace) -> None:
        """Set nodes attributes."""
        if isinstance(args.nodes, str):
            self.nodes = [args.nodes]
        elif isinstance(args.nodes, list):
            self.nodes = args.nodes

    def load_user_settings(self, args: Namespace) -> None:
        """Load additional settings from a json file."""
        if args.settings == '':
            home = pathlib.Path.home()
            settings_file = str(home.joinpath('.squiprc'))
        else:
            settings_file = str(pathlib.Path(args.settings))

        try:
            with open(settings_file, 'r') as s_file:
                content = s_file.read()
        except FileNotFoundError:
            pass
        else:
            self.apply_settings(content)

    def load_global_settings(self) -> None:
        """Load additional settings from a json file."""
        settings_file = str(pathlib.Path('/etc/.squiprc'))

        try:
            with open(settings_file, 'r') as s_file:
                content = s_file.read()
        except FileNotFoundError:
            pass
        else:
            self.apply_settings(content)

    def apply_settings(self, content: str) -> None:
        """Process content of settings file."""
        data = json.loads(content)
        for key, val in data.items():
            if key == 'colors':
                self.colors = {**self.colors, **val}
            elif key == 'nodes':
                assert isinstance(val, list)
                self.nodes += val
            elif key == 'scheme':
                if val in SCHEME_CHOICES:
                    self.scheme = val
                else:
                    print(InvalidKeyValueWarning(
                        key, val, SCHEME_CHOICES
                    ))
            elif key == 'user_scheme':
                if val in USER_CHOICES:
                    self.user_scheme = val
                else:
                    print(InvalidKeyValueWarning(
                        key, val, USER_CHOICES
                    ))
            elif key == 'empty_nodes':
                if isinstance(val, bool):
                    self.empty_nodes = val
                else:
                    print(InvalidKeyValueWarning(key, val, ('true', 'false')))
            elif key == 'users_jobs_only':
                if isinstance(val, bool):
                    self.mine_only = val
                else:
                    print(InvalidKeyValueWarning(key, val, ('true', 'false')))
            elif key == 'no_pending':
                if isinstance(val, bool):
                    self.pending = not val
                else:
                    print(InvalidKeyValueWarning(key, val, ('true', 'false')))
            elif key == 'no_status':
                if isinstance(val, bool):
                    self.status = not val
                else:
                    print(InvalidKeyValueWarning(key, val, ('true', 'false')))
            else:
                print(UnknownKeyWarning(key))
        self.nodes = list(set(self.nodes))

    def set_selectors(self, args: Namespace) -> None:
        """Set parameters for suppressed output."""
        if args.no_pending:
            self.pending = False

        elif args.no_status:
            self.status = False

        elif args.pending:
            self.print_nodes = False
            self.status = False

        elif args.status:
            self.print_nodes = False
            self.pending = False

        if args.mine_only:
            self.mine_only = True

        if args.no_empty:
            self.empty_nodes = False

        if args.no_jobs:
            self.pending = False
            self.show_jobs = False

    def set_debug(self, args: Namespace) -> None:
        """Set debug flag."""
        self.debug = args.debug
        if self.debug:
            self.enable_logging()

    def set_mocked(self, args: Namespace) -> None:
        """Set mocked flag."""
        self.mocked = args.mocked

    def set_logging(self, args: Namespace) -> None:
        """Set logging flag."""
        if args.logging:
            self.enable_logging()

    def enable_logging(self) -> None:
        """Activate propper logging function."""
        self.logger = utils.logger
        self.logging = True
