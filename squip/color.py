"""Provide Color class."""

import json
import importlib.resources
from typing import Dict

from .setting import Setting


class Color:
    """
    Color definitions for different elements (default settings).

    The presented strings and their combinations allow to change the color
    settings for the console output (xterm).
    """

    pre = '\033['
    bg = "48;5;"
    fg = "38;5;"
    suf = 'm'
    sep = ';'

    clr: Dict[str, str] = {}

    def __init__(self, setup: Setting) -> None:
        """Initialize Color instance."""
        self.load_scheme('base')
        self.load_scheme(setup.scheme)
        self.load_scheme(setup.user_scheme)
        self.load_usr_scheme(setup)

        self.clr['normal'] = self.get('normal_fg')
        self.clr['node_mix'] = self.get('node_mix_fg')
        self.clr['node_down'] = self.get('node_down_fg')
        self.clr['node_idle'] = self.get('node_idle_fg')
        self.clr['node_alloc'] = self.get('node_alloc_fg')
        self.clr['node_maintenance'] = self.get('node_maintenance_fg')
        self.clr['node_drain'] = self.get('node_drain_fg')
        self.clr['usr_job'] = self.get('usr_job_fg')
        self.clr['ram_top'] = self.get('ram_top_fg')
        self.clr['ram_high'] = self.get('ram_high_fg')
        self.clr['ram_med'] = self.get('ram_med_fg')
        self.clr['ram_low'] = self.get('ram_low_fg')
        self.clr['load_top'] = self.get('load_top_fg')
        self.clr['load_high'] = self.get('load_high_fg')
        self.clr['load_med'] = self.get('load_med_fg')
        self.clr['load_low'] = self.get('load_low_fg')
        self.clr['no_jobs_pending'] = self.get('no_jobs_pending_fg')

    @classmethod
    def get(cls, fgc: str) -> str:
        """Return string to change color in terminal."""
        return cls.pre + cls.fg + cls.clr[fgc] + cls.suf

    @classmethod
    def load_usr_scheme(cls, setup: Setting) -> None:
        """
        Load user defined colorscheme.

        Those are colors specified in the "colors" parameters of a
        configuration file.
        """
        for key, val in setup.colors.items():
            cls.clr[key] = val

    @classmethod
    def load_scheme(
        cls,
        scheme: str = 'dark'
    ) -> None:
        """
        Load user defined colorscheme.

        Colors loaded here are a part of one of the available  basic schemes:
        - dark
        - light

        and
        - user
        - root
        """
        filename = f'squip_{scheme}'
        with importlib.resources.open_text(
            'squip', filename
        ) as basethemefile:
            content = basethemefile.read()

        data = json.loads(content)
        for key, val in data.items():
            cls.clr[key] = val
