"""Provide the Memory class."""

from __future__ import annotations

from typing import Union

from .job import NOTAVAIL
from .color import Color


class Byte:
    """The Class for calculations with Bytes."""

    units = ('', 'K', 'M', 'G', 'T', 'P', 'E')

    def __init__(self, valid: bool = False) -> None:
        """Initialize Byte instance."""
        self.value: float = 0.0
        self.okay = valid

    def print(self, frmt: str = '5.1f') -> str:
        """
        Return human readable size of Memory (bytes).

        If 'okay' flag is not True "N/A" will be returned.
        When a Byte object was set correctly by the 'set' method
        this flag is set to 'True'.
        """
        if self.okay:
            val = self.value
            unit = 0
            while val > 1000:
                unit += 1
                val /= 1024

            return f'{val:{frmt}} {self.units[unit]}B'

        return " N/A "

    def add(self, value: Union[str, float, Byte], unit: str = 'M') -> Byte:
        """Add Bytes to an existing value."""
        if isinstance(value, Byte):
            if value.okay:
                self.value += value.value
            return self

        return self

    def get(self) -> float:
        """Get value as float."""
        return self.value

    def set(self, value: Union[str, float], unit: str = 'M') -> Byte:
        """Set Bytes with optional units."""
        try:
            value_float = float(value)
        except ValueError:
            pass

        else:
            factor = pow(1024, self.units.index(unit))
            self.value = value_float * factor
            self.okay = True

        return self


class Memory:
    """Handle free, allocated and total memory of a Node or Cluster."""

    def __init__(self, valid: bool = False) -> None:
        """Initialize Memory object."""
        self.alloc = Byte(valid)
        self.free = Byte(valid)
        self.total = Byte(valid)

    def used(self) -> Byte:
        """Return size of used memory."""
        return Byte().set(self.total.get() - self.free.get(), '')

    def get_ratio(self) -> str:
        """Return ratio of allocated memory in percent."""
        ratio = self.ratio()
        if isinstance(ratio, float):
            return '{:5.1f}'.format(ratio)
        return NOTAVAIL

    def ratio(self) -> Union[float, str]:
        """Get ratio of allocated memory."""
        try:
            return self.alloc.get() / self.total.get() * 100
        except (TypeError, ZeroDivisionError):
            return NOTAVAIL

    def get_color(self, color: str, colors: Color) -> str:
        """Return color for ratio depending on the value."""
        ratio = self.ratio()
        if ratio == NOTAVAIL:
            return color
        assert isinstance(ratio, float)
        if ratio > 90:
            col = colors.clr['ram_top']
        elif ratio > 80:
            col = colors.clr['ram_high']
        elif ratio > 50:
            col = colors.clr['ram_med']
        elif ratio > 1:
            col = colors.clr['ram_low']
        else:
            col = color
        return col

    def add(self, other: Memory) -> None:
        """Add memory to an existing instance."""
        self.total.add(other.total)
        self.alloc.add(other.alloc)
        self.free.add(other.free)
