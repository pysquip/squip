"""Provides utility functions."""

import os
import datetime
import re
import itertools
from typing import List, Set, Tuple

from . import config

RANGE_PATTERN = r'(\[[\d\,\-]+\])'


def dump_file(filename: str, *content: str) -> None:
    """
    Write content to file with filename.

    If filename already exists, it will be overwritten without warning.
    """
    if os.path.exists(filename):
        os.remove(filename)
    with open(filename, "w") as dumpfile:
        dumpfile.write("\n".join(content))


def logger(content: str) -> None:
    """Write content to log."""
    logentry = f'{datetime.datetime.now()}: {content}\n'
    with open(config.LOGFILE, "a") as log:
        log.write(logentry)


def dummy_logger(content: str) -> None:
    """Receive log directives and ignore."""
    content


def remove_old_log() -> None:
    """Remove existing logfile."""
    if os.path.exists(config.LOGFILE):
        os.remove(config.LOGFILE)


def resolve_node_range(string: str) -> str:
    """Return pipe seperated list of nodes from range."""
    nodelist_parts = _get_nodelist_parts(string)

    nodes: Set[str] = set()
    for part in nodelist_parts:
        string, ranges = _get_ranges(part.strip())
        nodelist_part = _put_together(string, ranges)
        nodes.update(nodelist_part)

    return "|".join(sorted(nodes)) + "|"


def _get_nodelist_parts(nodelist: str) -> List[str]:
    matches = _get_range_matches(nodelist)
    nodelist_with_placeholders = _get_string_with_placeholders(nodelist, matches)
    nodelist_parts_with_placeholders = nodelist_with_placeholders.split(',')

    nodelist_parts: List[str] = []
    for nodelist_part in nodelist_parts_with_placeholders:
        nodelist_parts.append(_get_string_without_placeholders(nodelist_part, matches))

    return nodelist_parts


def _get_ranges(string: str) -> Tuple[str, List[Set[int]]]:
    """
    Return components to assemble node ranges.

    The return value is a tuple containing the base node name
    with placeholders where the numbers have to be inserted that
    are given in form of a list of sets in the second element of
    the tuple.

    Example:

    input:
        'node-[0,2]-[3-5]'

    returns:
        (
            'node-MATCH0MATCH-MATCH1MATCH',
            [
                [0,2],
                [3,4,5]
            ]
        )
    """
    matches = _get_range_matches(string)
    string = _get_string_with_placeholders(string, matches)

    groups: List[Set[int]] = []
    for match in matches:
        if re.match(RANGE_PATTERN, match):
            group: Set[int] = set()
            match_stripped = match.strip('[]').split(',')
            for subgroup in match_stripped:
                bound = subgroup.split('-', 1)
                if len(bound) == 1:
                    group.add(int(*bound))
                else:
                    group.update(range(int(bound[0]), int(bound[1]) + 1))
            groups.append(group)
        else:
            raise ValueError

    return string, groups


def _get_string_with_placeholders(original: str, matches: List[str]) -> str:
    result = original
    for i, match in enumerate(matches):
        placeholder = f'MATCH{i}MATCH'
        result = result.replace(match, placeholder, 1)
    return result


def _get_string_without_placeholders(original: str, matches: List[str]) -> str:
    result = original
    for i, match in enumerate(matches):
        placeholder = f'MATCH{i}MATCH'
        result = result.replace(placeholder, match, 1)
    return result


def _get_placeholder(index: int) -> str:
    return f'MATCH{index}MATCH'


def _get_range_matches(nodelist: str) -> List[str]:
    return re.findall(RANGE_PATTERN, nodelist)


def _put_together(string: str, ranges: List[Set[int]]) -> Set[str]:
    """Assemble node list based on return value of get_ranges."""
    nodelist: List[str] = []
    for combi in itertools.product(*ranges):
        new_node = string
        for i, val in enumerate(combi):
            new_node = new_node.replace(_get_placeholder(i), str(val), 1)
        nodelist.append(new_node)

    return set(nodelist)
