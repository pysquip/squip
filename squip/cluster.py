"""Provide Cluster class."""

from __future__ import annotations

import os
import re
from squip.utils import dump_file
from typing import Dict, List, Optional

from .memory import Memory
from .cpu import Cpu
from .node import Node
from .setting import Setting
from .job import Job
from . import config
from . import utils


class Cluster:
    """Class to deal with information on the cluster."""

    def __init__(self) -> None:
        """Initialize Cluster instance."""
        self.ram = Memory(True)
        self.disk = Memory(True)
        self.cpu = Cpu(True)
        self.node = Cpu(True)
        self.job = Cpu(True)
        self.nodes: Dict[str, Dict[str, str]] = {}
        self.jobs: Dict[str, Dict[str, str]] = {}

    def get_slurm_data(self, setting: Setting) -> Cluster:
        """Call scontrol to get node and job data."""
        node_txt = self.get_node_data(setting.mocked)
        if setting.debug:
            dump_file(config.NODES_FILE, *node_txt)
        for i in node_txt:
            new = self.str_to_dict(i)
            if new is not None:
                self.nodes[new["NodeName"]] = new

        jobs_txt = self.get_job_data(setting.mocked)
        for i in jobs_txt:
            if setting.debug:
                dump_file(config.JOBS_FILE, *jobs_txt)
            new = self.str_to_dict(i)
            if new is not None:
                new['NodeList'] = utils.resolve_node_range(new['NodeList'])
                self.jobs[new["JobId"]] = new
        return self

    @classmethod
    def str_to_dict(cls, txt: str) -> Optional[Dict[str, str]]:
        """Convert string of key-value pairs to dict."""
        field_dict: Dict[str, str] = {}
        fields = re.findall(r"(?:([^\s]+=[^=]+)\s+)*?", txt)
        if fields is None:
            return None
        for grp in fields:
            match = re.match(r"(.*)=(.*)", grp)
            if match:
                field_dict[match.group(1)] = match.group(2)
        return field_dict

    @classmethod
    def get_node_data(cls, mocked: bool) -> List[str]:
        """Return lines from scontrol show node call."""
        if mocked:
            with open(config.NODES_FILE, "r") as nodesfile:
                content = nodesfile.readlines()
                content = list(line for line in content if line.strip())
            return content
        return os.popen(config.SLURM_NODES_CMD).readlines()

    @classmethod
    def get_job_data(cls, mocked: bool) -> List[str]:
        """Return lines from scontrol show job call."""
        if mocked:
            with open(config.JOBS_FILE, "r") as jobsfile:
                content = jobsfile.readlines()
                content = list(line for line in content if line.strip())
            return content
        return os.popen(config.SLURM_JOBS_CMD).readlines()

    def get_nodes_list(self, setup: Setting) -> List[str]:
        """Return list of nodes on cluster."""
        nodes = [
            i
            for i in self.nodes
            if any(re.match(pat, i.strip()) for pat in setup.nodes)
        ]
        return nodes

    def no_pending(self, setup: Setting) -> bool:
        """Return True if no pending jobs where found."""
        if setup.mine_only:
            for i in self.jobs.values():
                if (
                    i["JobState"].lower() == "pending"
                    and setup.user in i["UserId"]
                ):
                    return False
            return True

        for i in self.jobs.values():
            if i["JobState"].lower() == "pending":
                return False
        return True

    def get_pending(self) -> List[Job]:
        """Return list of pending jobs."""
        jobs: List[Job] = []
        for i in self.jobs.values():
            if i["JobState"].lower() == "pending":
                jobs.append(Job(i))
        return jobs

    def get_job_stats(self) -> None:
        """Set job for statistics."""
        pendings = 0
        runnings = 0
        for i in self.jobs.values():
            if i["JobState"].lower() == "pending":
                pendings += 1
            if i["JobState"].lower() == "running":
                runnings += 1
        self.job.set(str(runnings), str(runnings + pendings), "0")

    def add_node(self, node: Node) -> None:
        """Add a nodes resources to the cluster stats."""
        self.ram.add(node.ram)
        self.cpu.add(node.cpu)
        self.disk.add(node.disk)

        self.node.total += 1
        if node.state == "idle":
            self.node.free += 1

    def print_status(self, length: int) -> None:
        """Print the cluster status section."""
        self.get_job_stats()
        status_text = "C L U S T E R    S T A T U S"
        blank = " " * (length - 2)
        space = " " * int((length - 2 - len(status_text)) / 2)
        col_len = int((length - 12) / 5)
        missing = length - col_len * 5 - 12
        missingr = missing // 2 + (missing % 2 > 0)
        missingl = missing // 2
        padr = missingr * " "
        padl = missingl * " "
        frmt = f"^{col_len}"
        frmt_line = f"-^{col_len}"
        frmt_left = f">{int(col_len/2)}"
        frmt_right = f"<{int(col_len/2)}"
        print("#" * length)
        print(f"#{blank}#")
        print(f"#{space}{status_text}{space}#")
        print(f"#{blank}#")
        print(
            f"# {padl}"
            f'{"RAM":{frmt}} '
            f' {"CPUs":{frmt}} '
            f' {"Nodes":{frmt}} '
            f' {"Jobs":{frmt}} '
            f' {"HardDisk":{frmt}}'
            f"{padr} #"
        )
        print(
            f'# {padl}{"":{frmt_line}}  {"":{frmt_line}}  {"":{frmt_line}}  '
            f'{"":{frmt_line}}  {"":{frmt_line}}{padr} #'
        )
        print(
            f"# {padl}"
            f"{self.ram.alloc.print():{frmt_left}}"
            f' {"alloc.":{frmt_right}}'
            "  "
            f"{str(self.cpu.used()):{frmt_left}}"
            f' {"runn.":{frmt_right}}'
            "  "
            f"{str(self.node.used()):{frmt_left}}"
            f' {"runn.":{frmt_right}}'
            "  "
            f"{str(self.job.used()):{frmt_left}}"
            f' {"runn.":{frmt_right}}'
            "  "
            f'{"--":{frmt}}{padr} #'
        )
        print(
            f"# {padl}"
            f"{self.ram.free.print():{frmt_left}}"
            f' {"free":{frmt_right}}'
            "  "
            f"{str(self.cpu.free):{frmt_left}}"
            f' {"free":{frmt_right}}'
            "  "
            f"{str(self.node.free):{frmt_left}}"
            f' {"free":{frmt_right}}'
            "  "
            f"{str(self.job.free):{frmt_left}}"
            f' {"pend.":{frmt_right}}'
            "  "
            f'{"--":{frmt}}{padr} #'
        )
        print(
            f"# {padl}"
            f"{self.ram.total.print():{frmt_left}}"
            f' {"total":{frmt_right}}'
            "  "
            f"{str(self.cpu.total):{frmt_left}}"
            f' {"total":{frmt_right}}'
            "  "
            f"{str(self.node.total):{frmt_left}}"
            f' {"total":{frmt_right}}'
            "  "
            f"{str(self.job.total):{frmt_left}}"
            f' {"total":{frmt_right}}'
            "  "
            f"{self.disk.total.print():{frmt_left}}"
            f' {"total":{frmt_right}}{padr} #'
        )
        print(
            f'# {padl}{"":{frmt_line}}  {"":{frmt_line}}  {"":{frmt_line}}  '
            f'{"":{frmt_line}}  {"":{frmt_line}}{padr} #'
        )
        print(
            f"# {padl}"
            f"{self.ram.get_ratio():{frmt_left}}"
            f' {"% used":{frmt_right}}'
            "  "
            f"{self.cpu.used_ratio():{frmt_left}}"
            f' {"% used":{frmt_right}}'
            "  "
            f"{self.node.used_ratio():{frmt_left}}"
            f' {"% used":{frmt_right}}'
            "  "
            f"{self.job.used_ratio():{frmt_left}}"
            f' {"% run.":{frmt_right}}'
            "  "
            f'{"--":{frmt}}{padr} #'
        )
        print(f"#{blank}#")
