"""Provide CPU class."""

from __future__ import annotations

from .color import Color
from .job import NOTAVAIL


class Cpu:
    """CPU core statistic class."""

    def __init__(self, valid: bool = False) -> None:
        """Initialize Cpu instance."""
        self.free = 0
        self.load = .0
        self.total = 0
        self.valid = valid

    def used(self) -> int:
        """Return number of occupied cores."""
        return self.total - self.free

    def used_ratio(self) -> str:
        """Return ratio of used cores."""
        try:
            return '{:5.1f}'.format(self.used()/self.total*100)
        except ZeroDivisionError:
            return NOTAVAIL

    def get_color(self, color: str, colors: Color) -> str:
        """Return color depending on the load and number of cores."""
        if self.total == 0 or not self.valid:
            return color

        try:
            ratio = self.load/self.total
        except ZeroDivisionError:
            return color

        if ratio > 1:
            col = colors.clr['load_top']
        elif .8 < ratio < 1:
            col = colors.clr['load_high']
        elif .5 < ratio < .8:
            col = colors.clr['load_med']
        elif .08 < ratio < .5:
            col = colors.clr['load_low']
        else:
            col = color
        return col

    def set(self, alloc: str, total: str, load: str) -> None:
        """Set statistics/values for CPU."""
        try:
            cores = int(total)
        except ValueError:
            return

        try:
            occ_cpu = int(alloc)
        except ValueError:
            return

        try:
            load_float = float(load)
        except ValueError:
            return

        self.total = cores
        self.free = cores - occ_cpu
        self.load = load_float
        self.valid = True

    def print(self) -> str:
        """Print used vs total CPU."""
        if self.valid:
            return f'{self.total-self.free}/{self.total}'

        return ' N/A '

    def add(self, other: Cpu) -> None:
        """Add CPUs to an existing set."""
        if other.valid:
            self.total += other.total
            self.free += other.free
            # self.load += other.load
