"""Provide Node class."""

# import os
# import re
from typing import Dict, Optional

from .memory import Memory
from .cpu import Cpu
from .color import Color
from .job import Job, NOTAVAIL


class Node:
    """Everything related to nodes."""

    # string for the header in tabular presentation of nodes
    header = (
        f' {"HOSTNAME":9}'
        f' {"PARTITION":9}'
        f' {"RAM_RATIO":>10}'
        f'  {"ALLOC":>8} |{"FREE":>8} |{"TOTAL":>9}'
        f'{"HARDDISK":>13}'
        f' {"CPUs":>6}'
        f' {"CPULOAD":>8}'
        f'    {"STATE":8}'
        f' {"REASON":15}'
    )

    def __init__(self) -> None:
        """Initialize Node instance."""
        self.host = ''
        self.partition = ''
        self.ram = Memory()
        self.disk = Memory()
        self.cpu = Cpu()
        self.state = ''
        self.reason = ''
        self.jobs: Dict[str, Job] = {}

    def get_color(self, colors: Color) -> str:
        """Return color depending on the status of the node."""
        color = colors.clr['normal']
        if self.state.startswith('down'):
            color = colors.clr['node_down']
        elif 'drain' in self.state:
            color = colors.clr['node_drain']
            self.state = 'drain'
        elif self.state.startswith('idle'):
            color = colors.clr['node_idle']
        elif self.state.startswith('mix'):
            color = colors.clr['node_mix']
        elif self.state.startswith('alloc'):
            color = colors.clr['node_alloc']
        else:
            color = colors.clr['normal']

        if self.reason.startswith("Maintenance"):
            color = colors.clr['node_maintenance']

        self.reason = self.reason.partition('[')[0]

        return color

    def set_info(self, info: Dict[str, str]) -> Optional[bool]:
        """Set information for this node."""
        self.host = info['NodeName']
        self.partition = info['Partitions']
        self.state = info['State'].lower()
        self.reason = info.get('Reason', '')
        self.set_memory(
            info.get('TmpDisk', NOTAVAIL),
            info.get('AllocMem', NOTAVAIL),
            info.get('FreeMem', NOTAVAIL),
            info.get('RealMemory', NOTAVAIL),
        )
        self.set_cpu(
            info.get('CPUAlloc', NOTAVAIL),
            info.get('CPUTot', NOTAVAIL),
            info.get('CPULoad', NOTAVAIL)
        )
        return True

    def get_jobs(self, jobs: Dict[str, Dict[str, str]]) -> None:
        """Set dict with jobs on this node."""
        for key, val in jobs.items():
            if (
                self.host + '|' in val['NodeList']
                and val['JobState'].lower() == 'running'
            ):
                self.jobs[key] = Job(val)

    def no_jobs(self) -> bool:
        """Return True if no jobs are running on this node."""
        if len(self.jobs) > 0:
            return False
        return True

    def set_memory(
        self,
        disk: str,
        alloc: str,
        free: str,
        mem: str,
    ) -> None:
        """Extract Memory information from according strings."""
        self.ram.alloc.set(alloc)
        self.ram.free.set(free)
        self.ram.total.set(mem)
        self.disk.total.set(disk)

    def set_cpu(self, alloc: str, total: str, load: str) -> None:
        """Extract CPU information from according strings."""
        self.cpu.set(alloc, total, load)
