#!/usr/bin/env python3.7
"""Main module for showing a slurm queue and more."""

import os
from datetime import datetime

from .parser import argument_parser
from .node import Node
from .cluster import Cluster
from .color import Color
from .setting import Setting
from . import utils


def print_node(
    nodename: str,
    cluster: Cluster,
    colors: Color,
    length: int,
    setup: Setting,
) -> Cluster:
    """Process and print the node with the name 'nodename'."""
    #
    # read node information to variable var
    #
    node: Node = Node()
    stat = node.set_info(cluster.nodes[nodename])
    if stat is None:
        return cluster

    setup.logger('node:' + str(node.host))
    # add node data to cluster for final statistics
    cluster.add_node(node)

    #
    # Choose colors for node
    #
    node_color = node.get_color(colors)
    ram_color = node.ram.get_color(node_color, colors)
    cpu_color = node.cpu.get_color(node_color, colors)

    #
    # start with node header entry
    #
    string = [(
        node_color + " {:9} {:9}".format(node.host, node.partition)
        + ram_color + " {:>10}".format(f'{node.ram.get_ratio()} %')
        + node_color + " {:>9} |{:>8} |{:>9}{:>13}".format(
            node.ram.alloc.print(),
            node.ram.free.print(),
            node.ram.total.print(),
            node.disk.total.print(),
        )
        + " {:>6}".format(node.cpu.print())
        + cpu_color + " {:>8}".format(node.cpu.load)
        + node_color + "    {:<8} {:<15}".format(node.state, node.reason)
        + colors.clr['normal']
    )]

    # add jobs on that node to the output
    node.get_jobs(cluster.jobs)
    setup.logger('no jobs: ' + str(node.no_jobs()))
    if not node.no_jobs() and setup.show_jobs:
        string.append('- '*(int(length/2)))
        for i in node.jobs.values():
            if setup.user in i.user:
                color = colors.clr['usr_job']
            else:
                color = ''
            if (
                not setup.mine_only
                or
                (setup.mine_only and setup.user in i.user)
            ):
                string.append(color + i.to_str() + colors.clr['normal'])
    string.append('-'*(length))

    if (
        setup.print_nodes and (
            not node.no_jobs()
            or
            setup.empty_nodes
        )
    ):
        print("\n".join(string))

    return cluster


def print_nodes(
    cluster: Cluster,
    colors: Color,
    length: int,
    setup: Setting,
) -> Cluster:
    """Print the body of the table with all nodes."""
    nodes = cluster.get_nodes_list(setup)
    for node in nodes:
        cluster = print_node(node, cluster, colors, length, setup)
    return cluster


#
# PENDING JOBS section
#
def print_pending(
    setup: Setting,
    cluster: Cluster,
    colors: Color,
    length: int,
) -> None:
    """Print the section with pending jobs."""
    print('#'*length)
    string = "PENDING JOBS - "
    print('# '+(string * int(((length)/len(string)+1)))[:length-4]+' #')
    print('#'*length)

    if cluster.no_pending(setup):
        print(
            colors.clr['no_jobs_pending']
            + " no jobs pending"+colors.clr['normal']
        )
    else:
        pending = cluster.get_pending()
        for i in pending:
            color = ''
            getreason = False
            if setup.user in i.user:
                color = colors.clr['usr_job']
                getreason = True
            elif setup.user == 'root' or setup.as_root:
                getreason = True

            print(color + i.to_str(getreason)+"    "+colors.clr['normal'])


def print_color_code(colors: Color, length: int) -> None:
    """Print color code."""
    print('='*length)
    print(
        ' Colorcode: '
        + colors.clr['load_top'] + ' very high load '
        + colors.clr['load_high'] + ' high load '
        + colors.clr['load_med'] + ' medium load '
        + colors.clr['load_low'] + ' low load '
        + colors.clr['node_down'] + ' node down '
        + colors.clr['node_drain'] + ' node drain(ing) '
        + colors.clr['usr_job'] + ' current users jobs '
        + colors.clr['normal']
    )
    print('='*length)


def get_user() -> str:
    """Return the name of the user."""
    return os.popen('echo $USER').read().strip()


def main() -> None:
    """Driver."""
    usr = get_user()

    setup = argument_parser()
    setup.user = usr
    if setup.logging:
        utils.remove_old_log()

    colors = Color(setup)

    length = 120

    cluster = Cluster().get_slurm_data(setup)

    print(colors.clr['normal']+'#'*length)
    print(f' Timestamp:  {datetime.now().strftime("%Y-%m-%d  %H:%M:%S")}')
    print('='*length)
    if setup.print_nodes:
        print(Node.header)
        print('='*length)
    cluster = print_nodes(cluster, colors, length, setup)
    if setup.pending:
        print_pending(setup, cluster, colors, length)
    if setup.print_nodes:
        print_color_code(colors, length)
    if setup.status:
        cluster.print_status(length)
    print('#'*length)


if __name__ == '__main__':
    main()
