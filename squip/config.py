"""Constants."""

SLURM_NODES_CMD = 'scontrol show node -a -o'
SLURM_JOBS_CMD = 'scontrol show job -a -o'

NODES_FILE = 'scontrol_nodes.txt'
JOBS_FILE = 'scontrol_jobs.txt'

LOGFILE = 'squip.log'
