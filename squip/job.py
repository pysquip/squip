"""Provide the Job class."""

import re
from typing import Dict, Union
from datetime import datetime, timedelta

NOTAVAIL = "N/A"


class Job:
    """A Class for Jobs on a cluster."""

    def __init__(self, info: Dict[str, str]):
        """Initialize Job object."""
        self.user = info.get("UserId", NOTAVAIL).partition("(")[0]
        self.jobid = info.get("JobId", NOTAVAIL)
        try:
            self.status = info["JobState"].lower()
        except KeyError:
            self.status = NOTAVAIL
        self.name = info.get("JobName", NOTAVAIL)
        self.ram = info.get("MinMemoryNode", NOTAVAIL)
        self.cpu = info.get("MinCPUsNode", NOTAVAIL)
        self.start: Union[str, datetime] = self.parse_datetime(
            info.get("StartTime", NOTAVAIL)
        )
        self.runtime = self.parse_timedelta(info["RunTime"])
        self.submit: Union[str, datetime] = self.parse_datetime(
            info.get("SubmitTime", NOTAVAIL)
        )
        self.reason = info.get("Reason", NOTAVAIL)

    @classmethod
    def parse_datetime(cls, val: str) -> Union[datetime, str]:
        """Try to convert val to datetime object or return as is."""
        try:
            return datetime.fromisoformat(val)
        except ValueError:
            return val

    def get_submit(self) -> str:
        """Return string for the SubmitTime value."""
        if isinstance(self.submit, datetime):
            return str(self.submit.date())
        return self.submit

    @classmethod
    def parse_timedelta(cls, val: str) -> Union[timedelta, str]:
        """Try conversion of runtime string to timedelta object."""
        match = re.match(
            r"(:?(?P<days>[\d]+)\-)?(?P<hours>\d{1,2})?:(?P<mins>\d{1,2}):(?P<secs>\d{1,2})",
            val,
        )
        if match:
            try:
                i_days = float(match.group("days"))
            except (ValueError, TypeError):
                i_days = 0
            try:
                i_hrs = float(match.group("hours"))
            except (ValueError, TypeError):
                i_hrs = 0
            i_mins = float(match.group("mins"))
            i_secs = float(match.group("secs"))
            return timedelta(days=i_days, hours=i_hrs, minutes=i_mins, seconds=i_secs)
        return val

    def get_runtime(self) -> str:
        """Return string representation of runtime attribute."""
        return str(self.runtime)

    def to_str(self, reason: bool = False) -> str:
        """Return string for a tabular representation of a job."""
        if self.status.startswith("pending"):
            # pending string version
            status = "PD"
            time = f"     submit: {self.get_submit()}"
        elif self.status.startswith("running"):
            status = "R"
            time = f"{self.get_runtime():>24}   " f"start: {self.sensible_start():>15}"
        else:
            status = "ukwn"
            time = f"     submit: {self.get_submit()}"

        string = (
            f"{self.jobid:>10}"
            f" {self.name:<15.15}"
            f" {self.user:<13.13}"
            f" {status:>4}"
            f" {self.ram:>8} RAM"
            f" {self.cpu:>4} CPU"
            f" {time}"
        )
        if reason:
            max_len = 22
            string += f"    {self.get_reason(max_len):^{max_len}}"
        return string

    def sensible_start(self) -> str:
        """Adjust jobs start output precision to the time passed."""
        if isinstance(self.start, datetime):
            diff = datetime.now() - self.start
            if diff > timedelta(days=1):
                return self.start.strftime("%y-%m-%d  %H:%M")
            return self.start.strftime("%H:%M:%S")
        return self.start

    def get_reason(self, max_len: int) -> str:
        """Abbreviate reason and insert elipsis if too long."""
        if len(self.reason) >= max_len:
            return f"{self.reason[:max_len-3]}..."
        return self.reason
