"""Configuration of commandline arguments for squip."""

import argparse

from .setting import SCHEME_CHOICES, USER_CHOICES, Setting


def argument_parser() -> Setting:
    """Return parsed arguments."""
    parser = argparse.ArgumentParser(
        description='Slurm Queue In Pretty'
    )

    theme_base = parser.add_mutually_exclusive_group()

    for choice in SCHEME_CHOICES:
        theme_base.add_argument(
            f'--{choice}',
            action='store_true',
        )

    theme_user = parser.add_mutually_exclusive_group()

    for choice in USER_CHOICES:
        theme_user.add_argument(
            f'--{choice}',
            action='store_true',
        )

    parser.add_argument(
        '--nodes',
        action='store',
        help=(
            'regex expression for nodes to be scanned'
            ' (can be used more than once)'
        ),
    )

    parser.add_argument(
        '--no-empty', '--no-empty-nodes',
        action='store_true',
        help='hide empty nodes',
        dest='no_empty',
    )

    parser.add_argument(
        '-u', '--users-jobs-only',
        action='store_true',
        help='show only jobs of current user',
        dest='mine_only',
    )

    parser.add_argument(
        '-s', '--status',
        action='store_true',
        help='print only the status report',
    )

    parser.add_argument(
        '--no-status',
        action='store_true',
        help='exclude status report',
        dest='no_status',
    )

    parser.add_argument(
        '-p', '--pending',
        action='store_true',
        help='print only the list pending jobs'
    )

    parser.add_argument(
        '--no-pending',
        action='store_true',
        help='exclude list of pending jobs',
        dest='no_pending'
    )

    parser.add_argument(
        '--no-jobs',
        action='store_true',
        help='exclude jobs',
        dest='no_jobs'
    )

    parser.add_argument(
        '-f', '--settings-file',
        action='store',
        help='path of a provided settings file',
        default='',
        dest='settings',
    )

    parser.add_argument(
        '--as-root',
        action='store_true',
        help=argparse.SUPPRESS,
        dest='as_root',
    )

    parser.add_argument(
        '--debug',
        action="store_true",
        help='create files with information for bug report',
        dest='debug'
    )

    parser.add_argument(
        '--mocked',
        action="store_true",
        help='run with scontrol_* files as source instead of slurm',
        dest='mocked'
    )

    parser.add_argument(
        '--logging',
        action="store_true",
        help='enable logging (for debugging)',
        dest='logging'
    )

    args = parser.parse_args()

    setup = Setting(args)

    return setup
